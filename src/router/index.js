import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Project from '../views/Project.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    title: 'Der-Mitsch.de Homepage',
    name: 'Home',
    component: Home
  },
  {
    path: '/project/:projectId',
    title: 'Der-Mitsch.de Homepage',
    name: 'Project',
    component: Project,
    props: true,
  }
]

const router = new VueRouter({
  routes
})

export default router
