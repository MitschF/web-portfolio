import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    projects: [
      {
        id: 1,
        name: 'Connect Four',
        desc: 'Well-known game for two players. Group work from the second semester.',
        techs: ['Java, JavaFX'],
        repo: 'https://gitlab.com/MitschF/viergewinntse2',
        img: '4gewinnt1.png'
      },
      {
        id: 2,
        name: 'PLAN!',
        desc: 'Appointment calendar, todo list and shopping list in one application. Group work and semester project from the fourth semester.',
        techs: ['MongoDB', 'Express', 'VueJS', 'NodeJS'],
        repo: 'https://gitlab.com/MitschF/plan_mevn_frontend',
        img: 'plan_screenshot.jpg'
      },
      {
        id: 3,
        name: 'Crossy RPG!',
        desc: 'Simple "Cross the Road" type of game. First time using Python',
        techs: ['Python 3.8', 'Pygame',],
        repo: 'https://gitlab.com/MitschF/first-python-game',
        img: 'crossy-rpg.png'
      }
    ]
  },
  getters: {
    getProjectById: (state) => (id) => {
      return state.projects.find(project => project.id === id);
    }
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
